<?php
require __DIR__ . '/vendor/autoload.php';
require __DIR__ . '/db.inc.php';

$DEBUG = 0;

if (php_sapi_name() != 'cli') {
  throw new Exception('This application must be run on the command line.');
}

$options = getopt('v',['monthly-reset']);

if(array_key_exists('v',$options)) {
  $DEBUG=1;
}

showDebug("Cron run commences " . date("Y-m-d H:i:s"));

if(array_key_exists('monthly-reset', $options)) {
  monthlyReset();
}
else {
  syncWithSheet();
}

showDebug("Cron run ends " . date("Y-m-d H:i:s"));

/**
 * When called with '--monthly-reset' reset all monthly_impressions figures to 0
 * And update the monthly_impressions_last_reset to today's date
 */
function monthlyReset() {
  global $mysqli;

  $update = $mysqli->prepare('UPDATE ads SET monthly_impressions=0');
  $ex_status = $update->execute();
  if(!$ex_status) {
    exit("Reset monthly impressions failed to update ads table. " . $mysqli->error);
  }

  $update = $mysqli->prepare('UPDATE monthly_impressions_last_reset SET last_reset = CURDATE();');
  $ex_status = $update->execute();
  if(!$ex_status) {
    exit("Reset monthly impressions failed to log last update date. " . $mysqli->error);
  }

  $update->close();
  showDebug("Ethical ads: Monthly impressions reset.");
}


/**
 * When called with no options, sync the local MySQL db with the google "ad manager" sheet.
 */
function syncWithSheet() {
  global $spreadsheetId;
  global $mysqli;

  $client = getClient();
  $service = new Google_Service_Sheets($client);
  $range = 'Live ads!A2:P';
  $response = $service->spreadsheets_values->get($spreadsheetId, $range);
  $values = $response->getValues();

  if (empty($values)) {
    echo "No data found. Exiting.\n";
  }
  else {
    foreach ($values as $i => $row) {

      # silently skip rows with no name, id or zones
      if(
        !array_key_exists(0,$row) ||
        !array_key_exists(1,$row) ||
        !array_key_exists(2,$row)
      ) {
        continue;
      }

      # Skip rows with empty id
      $ad_id = $row[0];
      if($ad_id == '') {
        continue;
      }

      # An ad could have an empty name
      $name = $row[1];

      # An ad could show in no zones
      $zones = $row[2];

      $countries = "";
      if(array_key_exists(3, $row)) {
        $countries = $row[3];
      }
      #echo "$ad_id: $countries\n";

      $cities = '';
      if(array_key_exists(4, $row)) {
        $cities = $row[4];
      }

      $max_per_page = '';
      if(array_key_exists(5,$row) && (is_int((int)$row[5]) || $row[5]='')) {
        $max_per_page = $row[5];
      }

      $enabled = 0;
      if(array_key_exists(6,$row) && is_int((int)$row[6])) {
        $enabled = $row[6];
      }

      # If start date is empty, that means start immediately, set start date 'far in the past'
      $start = "1970-01-01 12:00:00";
      if(array_key_exists(7, $row) && $row[7] != '') {
        $start = $row[7];
      }

      # If expiry is empty, we don't want to expire. Set a date far in the future 2000 years should do it.
      $expiry = "4040-04-04 12:00:00";
      if(array_key_exists(8, $row) && $row[8] != '') {
        $expiry = $row[8];
      }

      $booked=0;
      if(array_key_exists(9, $row) && is_int((int)$row[9]) && $row[9] > 0) {
        $booked = $row[9];
      }

      $monthly_cap = 0;
      if(array_key_exists(10, $row) && is_int((int)$row[10]) && $row[10] > 0) {
        $monthly_cap = $row[10];
      }

      $shown=0;
      if(array_key_exists(11, $row) && is_int((int)$row[11]) && $row[11] > 0) {
        $shown = $row[11];
      }
      // showDebug("$ad_id shown: $shown\n");

      # We don't do much to check that the URL is sane in any way!
      $image_url = "MISSING IMAGE URL!!!";
      if(array_key_exists(12,$row) && $row[12] != '') {
        $image_url = $row[12];
        if(!preg_match('/https?:\/\//', $image_url)) {
          $image_url = "http://$image_url";
        }
      }

      $link_url  = "MISSING LINK URL!!!";
      if(array_key_exists(13,$row) && $row[13] != '') {
        $link_url = $row[13];
        if(!preg_match('/https?:\/\//', $link_url)) {
          $link_url = "http://$link_url";
        }
      }

      $clicks = 0;
      if(array_key_exists(14, $row) && is_int((int)$row[14]) && $row[14] > 0) {
        $clicks = $row[14];
      }

      $priority=0;
      if(array_key_exists(15, $row) && is_int((int)$row[15]) && $row[15] > 0 ) {
        $priority = $row[15];
      }

      $monthly_impressions = 0;

      $db_res = $mysqli->query('SELECT * FROM ads WHERE ad_id=' . $mysqli->escape_string($ad_id));
      $db_row = $db_res->fetch_assoc();

      if($db_row && $mysqli->affected_rows>1) {
        exit("More than one entry in DB for Ad ID $ad_id");
      }
      else if (!$db_row || $mysqli->affected_rows<1) {
        # We got a new entry
        $insert = $mysqli->prepare('INSERT INTO ads(ad_id, name, zones, countries, cities, max_per_page, enabled, start_date, expiry_date, booked, monthly_cap, monthly_impressions, shown, image_url, link_url, clicks, priority) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)');
        if(!$insert) {
          exit("Prepare insert stmt failed: (" . $mysqli->errno . ") " . $mysqli->error);
        }
        $bp = $insert->bind_param('issssiissiiiissii',
                $ad_id,
                $name,
                $zones,
                $countries,
                $cities,
                $max_per_page,
                $enabled,
                $start,
                $expiry,
                $booked,
                $monthly_cap,
                $monthly_impressions,
                $shown,
                $image_url,
                $link_url,
                $clicks,
                $priority
              )
              ;
        $ex_status = $insert->execute();
        if(!$ex_status) {
          exit("Insert statement failed for Ad ID $ad_id");
        }
        $insert->close();
        showDebug("Inserted new ad $ad_id");
        continue;
      }

      # We have exactly one row. So we need to update DB with all fields except clicks and impressions from sheet.
      # And google sheet with clicks and impressions from db.
      $update = $mysqli->prepare('UPDATE ads
        SET name=?,
          zones=?,
          countries=?,
          cities=?,
          max_per_page=?,
          enabled=?,
          start_date=?,
          expiry_date=?,
          booked=?,
          monthly_cap=?,
          image_url=?,
          link_url=?,
          priority=?
        WHERE ad_id=?');
      if(!$update) {
        exit("Prepare update stmt failed: (" . $mysqli->errno . ") " . $mysqli->error);
      }

      $bp = $update->bind_param('ssssiissiissii',
            $name,
            $zones,
            $countries,
            $cities,
            $max_per_page,
            $enabled,
            $start,
            $expiry,
            $booked,
            $monthly_cap,
            $image_url,
            $link_url,
            $priority,
            $ad_id
      );

      $sql = sprintf('UPDATE ads
        SET name=%s,
          zones=%s,
          countries=%s,
          cities=%s,
          max_per_page=%s,
          enabled=%s,
          start_date=%s,
          expiry_date=%s,
          booked=%s,
          monthly_cap=%s,
          image_url=%s,
          link_url=%s,
          priority=%s
          WHERE ad_id=%s',
            $name,
            $zones,
            $countries,
            $cities,
            $max_per_page,
            $enabled,
            $start,
            $expiry,
            $booked,
            $monthly_cap,
            $image_url,
            $link_url,
            $priority,
            $ad_id
        );
      # echo $sql;

      $ex_status = $update->execute();

      if(!$ex_status) {
        exit("Update statement failed for Ad ID $ad_id" . $mysqli->error);
      }
      $update->close();
      showDebug("Updated ad with id $ad_id");

      # We have to check for the presence of the clicks column in the case where
      # the user has not defined the columns before it as google truncates the values
      # array at the first empty column in the case where there are no further columns.
      # TODO: Is there a co-ordinate system for adding data to cells?
      if(array_key_exists(11, $values[$i])) {
        $values[$i][11] = (int)$db_row['shown'];
      }
      if(array_key_exists(14, $values[$i])) {
        $values[$i][14] = (int)$db_row['clicks'];
      }
      continue;

      $db_res->close();
    }

    $body = new Google_Service_Sheets_ValueRange([ 'values' => $values ]);
    $params = [ 'valueInputOption' => 'RAW' ];
    $result = $service->spreadsheets_values->update($spreadsheetId, $range, $body, $params);
  }
}

/**
 * Return an authorized API client.
 * @return Google_Client - the authorized client object
 */
function getClient() {
  $client = new Google_Client();
  $client->setApplicationName('Newint Ethical Ads Manager');
  $client->setScopes(Google_Service_Sheets::SPREADSHEETS);
  $client->setAuthConfig('credentials.json');
  $client->setAccessType('offline');
  $client->setPrompt('select_account consent');

  // Load previously authorized token from a file, if it exists.
  // The file token.json stores the user's access and refresh tokens, and is
  // created automatically when the authorization flow completes for the first
  // time.
  $tokenPath = 'token.json';
  if (file_exists($tokenPath)) {
    $accessToken = json_decode(file_get_contents($tokenPath), true);
    $client->setAccessToken($accessToken);
  }

  // If there is no previous token or it's expired.
  if ($client->isAccessTokenExpired()) {
    // Refresh the token if possible, else fetch a new one.
    if ($client->getRefreshToken()) {
      $client->fetchAccessTokenWithRefreshToken($client->getRefreshToken());
    } else {
      // Request authorization from the user.
      $authUrl = $client->createAuthUrl();
      printf("Open the following link in your browser:\n%s\n", $authUrl);
      print 'Enter verification code: ';
      $authCode = trim(fgets(STDIN));

      // Exchange authorization code for an access token.
      $accessToken = $client->fetchAccessTokenWithAuthCode($authCode);
      $client->setAccessToken($accessToken);

      // Check to see if there was an error.
      if (array_key_exists('error', $accessToken)) {
        throw new Exception(join(', ', $accessToken));
      }
    }
    // Save the token to a file.
    if (!file_exists(dirname($tokenPath))) {
      mkdir(dirname($tokenPath), 0700, true);
    }
    file_put_contents($tokenPath, json_encode($client->getAccessToken()));
  }
  return $client;
}

function showDebug($msg) {
  global $DEBUG;
  if($DEBUG) {
    echo($msg . "\n");
  }
}

?>
