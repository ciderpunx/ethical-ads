<?php
if(!array_key_exists('id', $_GET)) {
  exit();
}

$id = (int)$_GET['id'];

if(!$id) {
  exit();
}

// We check for automated link clicking just by looking at the UA string
// Obviously naive but we're not doing PPC so acceptable for now.
// Because we set rel=nofollow on links, the Bots shouldn't follow
$ua = $_SERVER['HTTP_USER_AGENT'];

if($ua) {
  $ignore_uas = [
      "Googlebot"
    , "Bingbot"
    , "Slurp"
    , "DuckDuckBot"
    , "Baduspider"
    , "YandexBot"
    , "Sogou"
    , "Exabot"
    , "Facebot"
    , "Facebookexternalhit"
    , "ia_archiver"
    , "crawler"
    , "Crawler"
    , "bot"
    , "Bot"
    , "curl"
    , "Wget"
    , "libwww-perl"
    ];

  foreach ($ignore_uas AS $ig) {
    if(strpos($ua, $ig) !== FALSE) {
      exit();
    }
  }
}

// Only require if needed (does this make any difference to performance or is it opcached away?)
require __DIR__ . '/db.inc.php';

$link_url = FALSE;
$id = $mysqli->escape_string($id);

// find the link url of the ad with id from the db
$db_sql = "SELECT link_url FROM ads WHERE id = $id";
$db_res = $mysqli->query($db_sql);
if(!$db_res) {
  die("Unable to query for ad with ID: $id");
}
if($db_row = $db_res->fetch_assoc()) {
 $link_url = $db_row['link_url'];
 if(!$link_url) {
  die("Link URL not in resultset for ad with ID: $id");
 }
}
else {
  die("No advert found in DB for ID $id");
}

// increment the click count for that id by 1
$db_sql = "UPDATE ads SET clicks = clicks+1 WHERE id = $id";
$db_res = $mysqli->query($db_sql);
if(!$db_res) {
  die("Unable to update click count for ad with ID: $id");
}

// 302 to that link_url
header("Location: $link_url");
exit();
