<?php
require __DIR__ . '/vendor/autoload.php';
use GeoIp2\Database\Reader;
header("Content-type: application/javascript");
if(!array_key_exists('zones', $_GET)) {
  js_err("No zones specified");
  exit();
}

$zone_ids = explode(',', $_GET['zones']);

$format='unwrapped';
if(array_key_exists('format', $_GET)) {
  if($_GET['format']=='w') {
    $format='wrapped';
  }
  else if($_GET['format']=='f') {
    $format='function';
  }
}
$prepend='';
if(array_key_exists('prepend', $_GET)) {
  if($_GET['prepend']=='google') {
    $prepend='div-gpt-ad-';
  }
}
$target='';
if(array_key_exists('target',$_GET)) {
  $target = $_GET['target']=='blank'
          ? ' target="_blank" '
          : '';
}

# $zone_ids = ["myzone1", "myzone3"];

if(count($zone_ids)>8) {
  # Too many zones, probably dodgy
  js_err("Too many zones (max 8)");
  exit();
}

require __DIR__ . '/db.inc.php';
$BASE_URL = 'https://ead.newint.org';

$ads_by_zone = array();
$ads_shown = array();

foreach($zone_ids as $i => $zone_id) {
  if(strlen($zone_id)>50) {
    #"Excessively long zone name encountered, probably dodgy";
    js_err("Zone name too long");
    exit();
  }

  ## TODO: Sufficient sanitization?
  $zone_id = $mysqli->escape_string($prepend . trim($zone_id));
  $zone_ids[$i] = $zone_id;

  $country_filter = '';
  $city_filter    = '';
  $region_filter  = ''; # TODO: Not implemented yet. May be more useful than city?
  $cityDbReader = new Reader('/usr/share/GeoIP/GeoLite2-City.mmdb');
  $geo_info = $cityDbReader->city($_SERVER['REMOTE_ADDR']);

  if($geo_info) {
    $country_filter = " AND (countries = '' OR countries LIKE '%{$geo_info->country->isoCode}%') ";
    $city_filter    = " AND (cities = '' OR cities LIKE '%{$geo_info->city->name}%') ";
    # echo "// $country_filter\n//$city_filter";
  }

  $db_sql = 'SELECT id, ad_id,name,image_url,shown,monthly_impressions,max_per_page,clicks,rand() as r
    FROM ads
    WHERE enabled=1
    AND (booked=0 OR shown<booked)
    AND (monthly_cap IS NULL OR monthly_cap=0 OR monthly_impressions<monthly_cap)
    AND expiry_date >= \'' . date("Y-m-d",time()) . "' " .
    ' AND start_date <= \'' . date("Y-m-d",time()) . "' " .
    $country_filter .
    $city_filter .
    ' AND zones LIKE \'%' . $zone_id . '%\'' .
    ' ORDER BY priority, r;'
  ;
  $db_res = $mysqli->query($db_sql);
  //echo "$db_sql\n";
  if(!$db_res) {
    # can't fetch results for zone, skip this one
    js_err("Can't fetch results for zone $zone_id");
    continue;
  }
  while($db_row = $db_res->fetch_assoc()) {
    $ad = array(
      'db_id'     => $db_row['id'],
      'ad_id'     => $db_row['ad_id'],
      'name'      => $db_row['name'],
      'image_url' => $db_row['image_url'],
      'link_url'  => "$BASE_URL/click.php?id={$db_row['id']}" ,
      'shown'     => $db_row['shown'],
      'monthly_impressions'
                  => $db_row['monthly_impressions'],
      'max_per_page'
                  => $db_row['max_per_page'],
      'clicks'    => $db_row['clicks']
    );
    $ads_by_zone[$zone_id][]= $ad;
    $ads_shown[$db_row['ad_id']]=0;
  }
  $db_res->free();
}

if(count($ads_shown)<1) {
  js_err("No suitable ads could be retrieved for any zone");
  exit();
}

echo ";//<!--Ethical Ads: Tracking-free ads-->\n";
if($format == 'wrapped') {
  echo "document.addEventListener(\"DOMContentLoaded\", function(event) {\n";
}
else if($format == 'function') {
  echo "function showEthicalAds() {\n";
}
foreach($zone_ids as $i => $zone_id) {
  # get first ad
  $ad_for_zone = FALSE;
  foreach($ads_by_zone[$zone_id] AS $ad) {
    if($ad['max_per_page']<1 || $ad['max_per_page']>$ads_shown[$ad['ad_id']]) {
      # got an ok ad. Show, update db and on to next zone;
      $ad_for_zone=TRUE;
      $ads_shown[$ad['ad_id']]++;
      js_for_ad($zone_id,$ad);
      db_show_ad($mysqli, $ad);
      break;
    }
  }
  if(!$ad_for_zone) {
    js_err("No ad available for zone $zone_id");
  }
}
if($format == 'wrapped') {
  echo "});\n";
}
else if($format == 'function') {
  echo "}\n";
}

$mysqli->close();


function js_err($msg) {
  echo "// ERR: $msg\nconsole.log(\"!!Ethicalads error: $msg\");\n";
}

function js_for_ad($zone_id,$ad) {
  global $target;
  $js_zone_id = preg_replace('/[^\-\w]+/i','-',$zone_id);
  $ad_markup = "<a class=\"no-hover\" rel=\"nofollow\" $target href=\"{$ad['link_url']}\"><img alt=\"{$ad['name']}\" src=\"{$ad['image_url']}\" /></a>";
  echo "\tif(window.document.getElementById('$js_zone_id')){window.document.getElementById('$js_zone_id').innerHTML='$ad_markup';}\n";
}

#TODO: shame to use multiple queries here
function db_show_ad($mysqli,$ad) {
  $new_shown = (int)$ad['shown']+1;
  $new_monthly_impressions = (int)$ad['monthly_impressions']+1;
  $db_sql = "UPDATE ads SET shown=$new_shown, monthly_impressions=$new_monthly_impressions WHERE ad_id={$ad['ad_id']};";
  $db_res = $mysqli->query($db_sql);
  if(!$db_res) {
    die("Failed to log ad impression");
  }
  return;
}
?>
