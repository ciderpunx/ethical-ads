# Ethical ads 

## Tracking free ads managed from a google sheet

### User instructions

You will manage your ads from the google sheet described in "Server 
installation" below. 

Basically you put an image on the web "somewhere", fill in a new row in the 
spreadsheet with the details of your ad and the URL of the image and (typically 
after 5-10 minutes) your ads will show. 

### Server installation

First you set up a google sheet, then run the cron.php to get a token.

You'll need to type your verification code in the terminal.

The sheet will have one ad per row in a sheet called "Live Ads" - the name is important.

The first row should be the field names.

The fields in the rows are are:
* Ad id  - A unique ID for the ad
* Ad name - A name for the ad
* Zones - Which Zones it should appear in - this is the DOM ids where you want 
          your ad to show.
* Countries - Which countries it should show in
* Cities - And which cities
* Max per page - If the ad can appear in multiple zones, this will limit the 
                 number of times it can appear on a page
* Enabled - 1 is on, 2 is off
* Expiry Date - Stop showing the ad after this date (YYY-MM-DD)
* Booked impressions - Stop showing the ad after this many impressions shown
* Shown impressions - How many impressions have been shown
* Image URL - The ad image
* Link URL - The ad link
* Clicks - Number of clicks this ad received
* Priority - This is used when more than one ad could appear in a zone, lower is
             more likely to appear

Once your sheet is ready, you need to set up a mysql database and user and add a 
table called ads


create table ads (id INT PRIMARY KEY AUTO_INCREMENT,
                  ad_id INT, 
                  name VARCHAR(255), 
                  zones VARCHAR(512), 
                  countries VARCHAR(512), 
                  enabled INT, 
                  expiry_date DATE, 
                  booked INT, 
                  shown INT, 
                  image_url VARCHAR(512), 
                  link_url VARCHAR(512), 
                  clicks INT);

Now, copy the db.inc.php.example to db.inc.php and add the deets for your new DB
to the mysqli definition.

You'll also need to change the spreadsheetId into the one you're using - which
you can get from the URL of the sheet.

Finally run cron.php once more and check that the data from your sheet ended up
in your mysql db.

You should set up a cron job to run cron.php every 5 or 10 minutes - this takes
care of syncing the google sheet with the database.

And you should set up php and nginx or apache to serve show.php and click.php.

### Client installation

The show.php script returns javascript for the zones you pass it with the zones 
parameter. Add it somewhere on each page on which you want ads shown.

For example:

https://example.com/show.php?zones=AdZone0,AdZone6

Would put ads in the DOM elements with ids AdZone0 and AdZone6. Some ad blockers
look at the names of these zones, so it's worth making them a bit generic. You 
may also want to rename show.php.

If you want a bit more control over the javascript you can have it return the 
code wrapped in a showEthicalAds function that you can call by setting the 
format param to 'f' for 'function', i.e.

https://example.com/show.php?zones=AdZone0,AdZone6&format=f

You can also have the javascript returned wrapped in an event listener that 
waits for DOMContentLoaded by setting format to 'w'.

If you want your ads to open in a new window, then set the target parameter to
'blank' and the linked ads will have target="_blank" added to their links.

There's a prepend parameter too, but it will most likely be deprecated - I was 
using it tot get round some adblocking restrictions.

### Bugs and such

Please report bugs in the gitlab issue tracker. Also feature requests.
