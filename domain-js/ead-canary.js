// Javascript for Ethicalads fallback for thecanary.co

function adFallback() {
  if(document.getElementsByClassName("google-auto-placed").length === 0) {
    // We did not encounter any google auto ads
    var zones = document.getElementsByClassName('in-content-ad');
    console.log(zones);
    if(zones.length > 0) {
      // We get the first in-content-ad div
      var zoneString  = 'canaryInTextAd1';
      zones[0].id=zoneString; // We call the first zone this because I hope it to be easy to understand
      zones[0].classList.remove('hide-for-medium'); // make it visible on larger devices
      zones[0].classList.remove('in-content-ad');   // classname blocked by EasyList
      zones[0].innerHTML='';
      var scriptElt   = document.createElement('script');
      scriptElt.type  = 'text/javascript';
      // This domain gets blocked by uBlock Origin
      // scriptElt.src   = "https://ads.thecanary.co/show.php?zones=" + zoneString;

      // This domain doesn't get blocked by uBlock Origin
      scriptElt.src   = "https://ead.newint.org/show.php?target=blank&zones=" + zoneString;
      document.body.appendChild(scriptElt);
    }
  }
}

window.addEventListener('load', function() {
  setTimeout (adFallback,500);
});
